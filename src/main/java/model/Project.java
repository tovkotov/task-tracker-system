package model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String title;
    private String description;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private User user;

    public Project(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Project() {
    }

    @Override
    public String toString() {
        return String.format("ID: %s | Title: %s | Description: %s",
                this.id, this.title, this.description);
    }
}
