package service;

import lombok.Data;
import menu.Action;
import menu.Message;
import model.Project;
import model.Task;
import model.User;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Scanner;

@Data
public class UserService {
    private static UserService instance;
    private List<User> userList;

    public static synchronized UserService getInstance() {
        if (instance == null)
            instance = new UserService();
        return instance;
    }

    public void printUsers() {
        System.out.println("User list: ");
        List<User> users = getUserList();
        if (users.isEmpty()) System.out.println("\tuser list is empty!");
        for (User user : users) {
            System.out.println("\t" + user);
//            getTasks(user);
        }
    }

    private void printTasks(User user) {
        List<Task> tasks = user.getTasks();
        for (Task task : tasks) System.out.println("\t\t" + task);
    }


    public List<User> getUserList() {
        String hql = "FROM User";
        TypedQuery<User> query = DbHandler.getInstance().getSession().createQuery(hql, User.class);
        return query.getResultList();
    }

    public User getUserByName(String userName) {
        for (User user : getUserList()) {
            if (user.getName().equals(userName)) {
                return user;
            }
        }
        return null;
    }

    public void deleteUser(User user) {
        String hql = "delete from User u where u.id = :id";
        Transaction transaction = DbHandler.getInstance().getSession().beginTransaction();
        Query query = DbHandler.getInstance().getSession().createQuery(hql);

        query.setParameter("id", user.getId());
        int result = query.executeUpdate();
        transaction.commit();
        System.out.println("Rows affected: " + result);
    }

    public User selectUser(String title) {
        User user = null;
        userList = getUserList();
        if (userList.isEmpty()) {
            System.out.println(Message.USER_LIST_EMPTY);
            editUser(Action.ADD);
            userList = getUserList();
        }
        Scanner in = new Scanner(System.in);
        System.out.println(title);
        int userNumber = 1;
        for (User element : userList) {
            System.out.println("\t" + userNumber++ + ") " + element.getName());
        }
        System.out.print(Message.ENTER_USER_NUMBER);
        String userNum = in.nextLine().trim();
        try {
            int num = Integer.parseInt(userNum);
            if ((num > userList.size()) || (num <= 0)) {
                System.out.println(Message.ERROR_NUMBER);
                //          continue;
                return null;
            }
            user = userList.get(num - 1);
        } catch (NumberFormatException ex) {
            System.out.println(Message.ASSIGN_SKIP);
        }
        return user;
    }


    public void editUser(int menuId) {
        if (menuId == Action.ADD) addUser();
        if (menuId == Action.DEL) deleteUser();
        if (menuId == Action.ASSIGN) assign();
    }

    private void assign() {
        User user = selectUser(Message.SELECT_USER);
        if (user == null) return;
        Project project = ProjectService.getInstance().selectProject(Message.SELECT_PROJECT);
        if (project == null) return;
        Transaction transaction = DbHandler.getInstance().getSession().beginTransaction();
        project.setUser(user);
        transaction.commit();
        System.out.println(Message.ASSIGN_USER_EXECUTE);
    }

    private void deleteUser() {
        User user = selectUser(Message.DELETE_USER);
        deleteUser(user);
    }

    private void addUser() {
        Scanner in = new Scanner(System.in);
        System.out.print(Message.ENTER_USER_NAME);
        String inText = in.nextLine().trim();
        DbHandler.getInstance().addUser(new User(inText));
        System.out.println(Message.USER_ADDED);
    }
}
