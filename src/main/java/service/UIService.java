package service;

import lombok.Data;
import menu.*;
import org.json.simple.parser.ParseException;

import java.util.Scanner;

@Data
public class UIService {
    private static UIService instance;

    private MainMenu mainMenu;
    private ShowMenu showMenu;
    private EditMenu editMenu;

    private String inText;

    public static synchronized UIService getInstance() {
        if (instance == null)
            instance = new UIService();
        return instance;
    }

    public void startApp() {
        inText = "";
        Scanner in = new Scanner(System.in);

        mainMenu = new MainMenu();
        showMenu = new ShowMenu();
        editMenu = new EditMenu();

        while (!inText.equals("q")) {
            mainMenu.show();
            try {
                inText = in.nextLine().trim();
                switch (inText) {
                    case "1" -> inputData();
                    case "2" -> viewData();
                    case "q" -> {
                        DbHandler.getInstance().closeConnection();
                        System.out.println(Message.EXIT_PROGRAM);
                    }
                    default -> {
                        System.out.println(Message.UNKNOWN_COMMAND);
                        mainMenu.show();
                    }
                }
            } catch (RuntimeException e) {
                DbHandler.getInstance().closeConnection();
                e.printStackTrace();
            }
        }
    }

    private void viewData() {
        inText = "";
        Scanner in = new Scanner(System.in);
        while (!inText.equals("q")) {
            showMenu.show();
            try {
                inText = in.nextLine().trim();
                switch (inText) {
                    case "1" -> UserService.getInstance().printUsers();
                    case "2" -> ProjectService.getInstance().printProjects();
                    case "3" -> TaskService.getInstance().printTasks();
                    case "4" -> TaskService.getInstance().printTasksByProjectAndUser();
                    case "q" -> startApp();
                    default -> System.out.println(Message.UNKNOWN_COMMAND);
                }
            } catch (RuntimeException e) {
                DbHandler.getInstance().closeConnection();
                e.printStackTrace();
            }
        }
    }

    public void inputData() {
        inText = "";
        Scanner in = new Scanner(System.in);
        int menuTab = Action.ADD;
        while (!inText.equals("q")) {
            editMenu.show(menuTab);
            try {
                inText = in.nextLine().trim();
                switch (inText) {
                    case "1" -> UserService.getInstance().editUser(menuTab);
                    case "2" -> ProjectService.getInstance().editProject(menuTab);
                    case "3" -> TaskService.getInstance().editTask(menuTab);
                    case "l" -> loadInitialData();
                    case "n" -> menuTab = menuTab == Action.ADD ?
                            Action.DEL : menuTab == Action.DEL ?
                                    Action.ASSIGN : Action.ADD;
                    case "q" -> startApp();
                    default -> System.out.println(Message.UNKNOWN_COMMAND);
                }
            } catch (RuntimeException | ParseException e) {
                DbHandler.getInstance().closeConnection();
                e.printStackTrace();
            }
        }
    }

    private void loadInitialData() throws ParseException {
        if (!UserService.getInstance().getUserList().isEmpty()) {
            System.out.println(Message.DATABASE_IS_NOT_EMPTY);
            mainMenu.show();
            return;
        }
        JsonParseService jsonParseService = new JsonParseService();
        jsonParseService.loadInitialData();
        System.out.println(Message.INITIAL_DATA);
    }
}
