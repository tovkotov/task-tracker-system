package service;

import lombok.Data;
import menu.Action;
import menu.Message;
import model.Project;
import model.Task;
import model.User;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Scanner;

@Data
public class TaskService {
    private static TaskService instance;

    public static synchronized TaskService getInstance() {
        if (instance == null)
            instance = new TaskService();
        return instance;
    }

    public void printTasks() {
        System.out.println("Task list: ");
        List<Task> tasks = getTaskList();
        if (tasks.isEmpty()) System.out.println("\ttask list is empty!");
        for (Task task : tasks) {
            System.out.println("\t" + task);
        }
    }

    public List<Task> getTaskList() {
        String hql = "FROM Task ";
        TypedQuery<Task> query = DbHandler.getInstance().getSession().createQuery(hql, Task.class);
        return query.getResultList();
    }

    public List<Task> getTaskListByProjectIdAndStatus(int projectId, int active) {
        String hql = "FROM Task T WHERE T.project.id =" + projectId + " AND T.active =" + active;
        TypedQuery<Task> query = DbHandler.getInstance().getSession().createQuery(hql, Task.class);
        return query.getResultList();
    }

    public void deleteTask() {
        Task task = selectTask(null, false);
        String hql = "delete from Task t where t.id = :id";
        Transaction transaction = DbHandler.getInstance().getSession().beginTransaction();
        Query query = DbHandler.getInstance().getSession().createQuery(hql);
        query.setParameter("id", task.getId());
        int result = query.executeUpdate();
        transaction.commit();
        System.out.println("Rows affected: " + result);
    }

    public void editTask(int menuId) {
        if (menuId == Action.ADD) addTask();
        if (menuId == Action.DEL) deleteTask();
        if (menuId == Action.ASSIGN) assign();
    }

    private void assign() {
        Project project = ProjectService.getInstance().selectProject(Message.SELECT_PROJECT);
        if (project == null) return;

        Task task = selectTask(project, false);
        if (task == null) return;

        User user = UserService.getInstance().selectUser(Message.SELECT_USER);
        if (user == null) return;

        Transaction transaction = DbHandler.getInstance().getSession().beginTransaction();
        task.setUser(user);
        task.setProject(project);

        transaction.commit();
        System.out.println(Message.ASSIGN_TASK_EXECUTE);
    }

    private void addTask() {
        Scanner in = new Scanner(System.in);
        Task task = new Task();
        System.out.print(Message.ENTER_TITLE);
        task.setTitle(in.nextLine().trim());
        System.out.print(Message.ENTER_DESCRIPTION);
        task.setDescription(in.nextLine().trim());

        task.setUser(UserService.getInstance().selectUser(Message.ASSIGN_USER));

        Project project = ProjectService.getInstance().selectProject(Message.ASSIGN_PROJECT);
        task.setProject(project);

        task.setParent(selectTask(project, true));
        task.setActive(true);

        DbHandler.getInstance().addTask(task);
        System.out.println(Message.TASK_ADDED);
    }

    private Task selectTask(Project project, boolean subtask) {
        Task task = null;
        String text = subtask ? Message.SELECT_PARENT_TASK : Message.SELECT_TASK;
        String print = project != null ?
                text :
                Message.SELECT_TASK_FOR_DELETE;
        List<Task> tasks = project != null ?
                TaskService.getInstance().getTaskListByProjectIdAndStatus(project.getId(), 1) :
                TaskService.getInstance().getTaskList();
        if (tasks.isEmpty()) return null;
        Scanner in = new Scanner(System.in);
        int taskNumber = 1;
        System.out.println(print);
        for (Task element : tasks) {
            System.out.println("\t" + taskNumber++ + ") " + element.getTitle());
        }
        System.out.print(Message.ENTER_TASK_NUMBER);
        String taskNum = in.nextLine().trim();
        try {
            int num = Integer.parseInt(taskNum);
            if ((num > tasks.size()) || (num <= 0)) {
                System.out.println(Message.ERROR_NUMBER);
                return null;
            }
            task = tasks.get(num - 1);
        } catch (NumberFormatException ex) {
            System.out.println(Message.ASSIGN_SKIP);
        }
        return task;
    }

    public void printTasksByProjectAndUser() {
        User user = UserService.getInstance().selectUser(Message.SELECT_USER);
        if (user == null) {
            System.out.println(Message.USER_HAVE_NOT_PROJECTS);
            return;
        }

        List<Project> projectList = ProjectService.getInstance().getProjectListByUser(user);
        if (projectList == null) {
            System.out.println(Message.PROJECT_LIST_IS_EMPTY);
            return;
        }

        System.out.println("user " + user.getName() + " has the following projects:");
        for (Project project : projectList) {
            System.out.println("\tTitle the project: " + project.getTitle());
            System.out.println("\t\tDescription: " + project.getDescription());
            List<Task> taskList = getTaskListByProjectIdAndStatusAndUser(project.getId(), 1, user);
            for (Task task : taskList) {
                System.out.println("\t\t- " + task.getTitle() + " " + task.getDescription());
            }
        }
        System.out.println();
    }

    private List<Task> getTaskListByProjectIdAndStatusAndUser(int id, int status, User user) {
        String hql = "FROM Task T WHERE T.project.id =" + id + " AND T.active =" + status + " AND T.user.id = " + user.getId();
        TypedQuery<Task> query = DbHandler.getInstance().getSession().createQuery(hql, Task.class);
        return query.getResultList();
    }
}
